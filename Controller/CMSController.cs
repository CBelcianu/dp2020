﻿using ConferenceManagementSystem.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using ConferenceManagementSystem.Domain;
using ConferenceManagementSystem.Service;

namespace ConferenceManagementSystem.Controller
{
    public class CMSController
    {
        private CMSService service = CMSService.Instance;
        public void addSection(string name, string room, DateTime date, int confId, int chairId)
        {
            /*
             * adds a section in the Sections table
             * pre: conference name (string), room (string), date (DateTime), conference id (integer), chair id (integer)
             * post: -
             */

            this.service.addSection(name, room, date, confId, chairId);
        }

        public void deleteSection(int id)
        {
            /*
             * deletes the section with a given id from the Sections table
             * pre: section id (integer)
             * post: -
             */
            this.service.deleteSection(id);
        }

        public void addReview(int paperId, int reviewerId, string qualifier, string comments)
        {
            /*
             * adds a new review in the Reviews table
             * pre: the paper id (integer), the reviewer id (integer), the qualifier (string), the comments (string)
             * post: throws an exception if the given paper already has the maximum number of reviews (4)
             */
            this.service.addReview(paperId, reviewerId, qualifier, comments);
        }
        public void AddConference(string ConferenceName, string ConferenceAddress, DateTime ConferenceDate)
        {
            /*
             * adds a new conference in the Conferences table
             * pre: conference name (string), conference address (string), conference date (DateTime)
             * post: -
             */
            this.service.AddConference(ConferenceName, ConferenceAddress, ConferenceDate);
        }

        public List<ChosenPcMember> getChosen()
        {
            /*
             * gets all the chosen PC Members from the ChosenPCMembers table
             * pre: -
             * post: a list with all the chosen PC Members
             */
            return this.service.getChosen();
        }

        public List<Log> getLogs()
        {
            return this.service.getLogs();
        }

        public void addChosen(string email, string role)
        {
            /*
             * adds a new chosen PC Member in the ChosenPC table
             * pre: email (string), role (string)
             * post: -
             */
            this.service.addChosen(email, role);
        }

        public void deleteChosen(string email)
        {
            /*
            deletes a chosen PC Member from the ChosenPC table
            pre: email (string)
            post: -
             */
            this.service.deleteChosen(email);
        }

        public void addPaper(string PaperName, string Topic, string ContentLoc, string AbstractLoc, int SectionID, int AuthorID, int RoleID)
        {
            /*
             * adds a new paper in the Papers and AuthorPapers tables
             * pre: paper name (string), topic (string), the path in the disc of the paper (string), the content on the disc of the abstract (string), the id of the section for the paper (integer), the author id (integer)
             * post: -
             */
            this.service.addPaper(PaperName, Topic, ContentLoc, AbstractLoc, SectionID, AuthorID, RoleID);
        }

        public void updateListener(int id, string firstName, string lastName)
        {
            /*
             * updates the listener with the given id in the Users table
             * pre: listener id(int), first name (string), last name (string)
             * post: -
             */
            this.service.updateListener(id, firstName, lastName);
        }

        public void updateAuthor(int id, string firstName, string lastName, string affiliation)
        {
            /*
             * updates the author with the given id in the Authors and Users tables
             * pre: author id (int), first name (string), last name (string), affiliation (string)
             * post: -
             */
            this.service.updateAuthor(id, firstName, lastName, affiliation);
        }

        public void updatePCMember(int id, string firstName, string lastName, string affiliation, string website)
        {
            /*
             * updates the PC Member with the given id in the Users and PcMembers tables
             * pre: PC Memeber id (integer), first name (string), last name (string), affiliation (string), website (string)
             * post: -
             */
            this.service.updatePCMember(id, firstName, lastName, affiliation, website);
        }

        public void attendConference(Conference conference, User user)
        {
            /*
             * adds the user id and the conference id in the ConferenceUser table 
             * pre: conference and user
             * post: throws an exception if the user is already marked as attending the given conference
             */
            this.service.attendConference(conference, user);
        }

        public List<Paper> getPapers()
        {
            /*
             * gets all the papers from the DB
             * pre: -
             * post: returns a list with all the papers
             */
            return this.service.getPapers();
        }

        public List<Paper> getPapersOfSection(Section section)
        {
            /*
             * gets all the papers from a given section from the DB
             * pre: a section (Section)
             * post: returns a list with all the papers from the given section
             */
            return this.service.getPapersOfSection(section);
        }

        public List<Conference> getConferences()
        {
            /*
             * get all the conferences from the DB
             * pre: -
             * post: returns a list with all the conferences
             */
            return this.service.getConferences();
        }

        public List<Section> getSections(int confId)
        {
            /*
             * gets all the sections with the given conference id from the Sections table
             * pre: conference id (integer)
             * post: a list with all the sections from the conference
             */
            return this.service.getSections(confId);
        }

        public List<Section> getSections() {
            /*
             * gets all the sections from the DB
             * pre: -
             * post: returns a list with all the sections
             */
            return this.service.getSections();
        }

        public void updateSectionDeadline(int id, DateTime newDate) {
            /*
             * updates the PaperDeadline field for the sections with the given id from the table Sections
             * pre: the section id (int) and the new deadline (DateTime)
             * post: -
             */
            this.service.updateSectionDeadline(id, newDate);
        }

        public List<Section> getSectionsOfConference(Conference conference)
        {
            /*
             * gets all the sections from a given conference
             * pre: a conference (Conference)
             * post: a list with all the sections 
             */
            return this.service.getSectionsOfConference(conference);
        }

        public List<Conference> getMyConferences(User user)
        {
            /*
             * gets all the conferences which a given user participates in
             * pre: a user (User)
             * post: a list with all the conferences
             */
            return this.service.getMyConferences(user);
        }

        public User LogIN(string username, string password)
        {
            /*
             * gets the user with the given username and password, and returns the user based on the type it has
             * pre: the username (string) and the password(string) of the user
             * post: the user, if the id is found, or throws an exception otherwise
             */
            return this.service.LogIN(username, password);
        }

        public void registerListener(string username, string passwd, string fname, string lname, string email)
        {
            /*
             * adds a new Listener in the Users table
             * pre: user username (string), password (string), first name (string), last name (string), email (string)
             * post: throws an exception if the username or the email are already used
             */
            this.service.registerListener(username, passwd, fname, lname, email);
        }

        public void registerAuthor(string username, string passwd, string fname, string lname, string email, string affiliation)
        {
            /*
             * adds a new author in the Users and Authors tables
             * pre: authors' username (string), password (string), first name (string), last name (string), email (string) and affiliation (string)
             * post: throws an exception if the username or the email are already used
             */
            this.service.registerAuthor(username, passwd, fname, lname, email, affiliation);
        }

        public List<Review> GetReviewsForPaper(Paper paper)
        {
            return this.service.GetReviewsForPaper(paper);
        }

        public List<Paper> getAcceptedPapers()
        {
            return this.service.getAcceptedPapers();
        }

        public void acceptPaper(Paper paper)
        {
            this.service.getAcceptedPapers();
        }

        public void reevalPaper(Paper paper)
        {
            this.service.reevalPaper(paper);
        }

        public void registerPCMember(string username, string passwd, string fname, string lname, string email, string affiliation, string website)
        {
            /*
             * adds a new PCMember to the Users and the PCMembers tables
             * pre: username (string), password (string), first name (string), last name (string), email (string), affiliation (string), website(string)
             * post: throws exception if the username or email are already used, or if the user does not have the right to register as a PCMember
             */
            this.service.registerPCMember(username, passwd, fname, lname, email, affiliation, website);
        }
    }
}
