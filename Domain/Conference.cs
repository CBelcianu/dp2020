﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConferenceManagementSystem.Domain
{
    public interface IConference
    {
        void log();
    }
    public class Conference: IConference
    {
        public int ID { get; set; }
        public string ConferenceName { get; set; }
        public string ConferenceAddress { get; set; }
        public string ConferenceDate { get; set; } 

        public void log()
        {
            string body = "ADDED conference with id " + this.ID.ToString();

            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["cmsDatabase"].ConnectionString))
            {
                try
                {
                    String query = "INSERT INTO Logs VALUES ('" + body + "'" + ")";
                    db.Execute(query);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

    }
}
