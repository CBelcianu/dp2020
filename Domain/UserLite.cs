﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConferenceManagementSystem.Domain
{
    public class UserLite
    {
        public string FirstName { get; set; }
        public string email { get; set; }
        public int RoleID { get; set; }
    }
}
