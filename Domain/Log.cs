﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConferenceManagementSystem.Domain
{
    public class Log
    {
        public int ID { get; set; }
        public string Msg { get; set; }
    }
}
